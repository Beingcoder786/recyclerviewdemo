package com.example.recyclerviewdemo

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlin.random.Random
class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val insertItem = findViewById<Button>(R.id.insertId)
        val removeItem = findViewById<Button>(R.id.removeId)
        val songList: androidx.recyclerview.widget.RecyclerView = findViewById(R.id.songList)
        val moviesObject = mutableListOf(
                Song("Kota Factory", "☆8.0"),
                Song("Family Man", "☆7.9"),
                Song("Devdas", "☆8.0"),
                Song("The Sky Is Pink", "☆7.9"),
                Song("Ratsasan", "☆5"),
                Song("3 idiots", "☆6"),
                Song("Dj", "☆7"),
                Song("Joker", "☆8.5"),
                Song("chichore", "☆8.2"),
                Song("super 30", "☆8.2"),
                Song("once upon a time in mumbai", "☆7.9"),
                Song("super Deluxe", "☆8.0"),
                Song("KGF", "☆9.0")
        )

        /* moviesObject.add(Song("Devdas", "☆8.0"))
         moviesObject.add(Song("The Sky Is Pink", "☆7.9"))
         moviesObject.add(Song("Ratsasan", "☆5"))
         moviesObject.add(Song("3 idiots", "☆6"))
         moviesObject.add(Song("Dj", "☆7"))
         moviesObject.add(Song("Joker", "☆8.5"))
         moviesObject.add(Song("chichore", "☆8.2"))
         moviesObject.add(Song("super 30", "☆8.2"))
         moviesObject.add(Song("once upon a time in mumbai", "☆7.9"))
         moviesObject.add(Song("super Deluxe", "☆8.0"))
         moviesObject.add(Song("KGF", "☆9.0"))*/

        val adapter = MyAdapter(moviesObject)
        songList.adapter = adapter
        songList.layoutManager = LinearLayoutManager(this)


        insertItem.setOnClickListener {

            val index: Int = Random.nextInt(moviesObject.size - 1)
            moviesObject.add(index, Song("Avenger", "☆10.0"))
            adapter.notifyDataSetChanged()

        }
        removeItem.setOnClickListener {
            val index: Int = Random.nextInt(moviesObject.size - 1)
            moviesObject.removeAt(index)
            adapter.notifyDataSetChanged()
        }
    }
}
