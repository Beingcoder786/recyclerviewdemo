package com.example.recyclerviewdemo

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class MyAdapter(private val moviesObject: List<Song>) : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_view, parent, false)
        return MyViewHolder(view)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // holder.bind(songs[position])
        holder.txtTitle.text = moviesObject[position].title
        holder.txtDescription.text = moviesObject[position].description
        var color = "#FFFFFF"
        if (position % 2 == 0) {
            color = "#A9A9A9"
        }
        holder.container.setBackgroundColor(Color.parseColor(color))
        holder.txtDescription.setTextColor(Color.parseColor("#FFCC00"))
        holder.txtTitle.setTextColor(Color.parseColor("#000000"))

        holder.container.setOnClickListener {
            Toast.makeText(holder.container.context, moviesObject[position].description, Toast.LENGTH_SHORT).show()
        }


    }

    override fun getItemCount(): Int {
        return moviesObject.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        /*fun bind(song: Song) {
           TODO("Not yet implemented")
        }*/

        var txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
        var txtDescription: TextView = itemView.findViewById(R.id.txtDescription)
        var container: LinearLayout = itemView.findViewById(R.id.container)


    }
}


